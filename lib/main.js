// Import the page-mod API
var pageMod = require("sdk/page-mod");
 var self = require("sdk/self");

// Create a page mod
// It will run a script whenever a ".org" URL is loaded
// The script replaces the page contents with a message
pageMod.PageMod({
  include: "*.melicloud.com",
  contentScriptWhen: 'ready',
  contentScriptFile: [self.data.url("jquery-1.11.0.min.js"), self.data.url("MeliConsole.js")],
  contentStyleFile: self.data.url("MeliConsole.css")
});